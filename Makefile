CC = icc
CFLAGS = -O3 -Wall
LDFLAGS = -mkl
MICFLAGS = -mmic
 
BINS = scan pscan iter

MIC_BINS = iter_mic

build: $(BINS)
mic: $(MIC_BINS)
 
$(BINS): %: src/%.c
	$(LINK.c) -o $@ $^

$(MIC_BINS): %: src/%.c
	$(LINK.c) $(MICFLAGS) -o $@ $^
	scp $(MIC_BINS) mic0:

clean:
	@rm -vf $(BINS) $(MIC_BINS)
