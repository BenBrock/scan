
/*

iter.c

Benjamin Brock
May, 2014

Iterative implementation of a patterns-based
parallel scan.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <CL/cl.h>
#include "clutils.h"
#include <chrono>
#include <iostream>
#include <cassert>

typedef struct stack {
  unsigned int size;
  int chunk_size;
  char *mem;
  char *sp;
} stack;

void stack_init(stack *s, int n, int cs)
{
  int i;

  s->size = 0;

  for (i = n; i >= cs; i /= cs) {
    s->size += i * sizeof(int);
  }
}

int main(int argc, char **argv)
{
  unsigned long n;
  int i, j, k, l;
  int z;
  int *x, *seq, *rv;
  unsigned int zero;
  stack *s;
  struct stat buf;
  FILE *f;
  char *k_source;

  double min, min_int, min_deint, min_k1, min_k2;
  int best_chunk;

  zero = 0;

  /* OpenCL stuff. */

  cl_mem d_x, d_mem;

  cl_platform_id *platforms;
  cl_device_id device;
  cl_context context;
  cl_command_queue queue;
  cl_program program;
  cl_kernel pscan_up, pscan_down, interleave, uninterleave;
  cl_uint p_count;
  cl_int err;
  size_t global_size, local_size;

  /* Initialization */

  char *fname = "iter_kernel.cl";

  clGetPlatformIDs(0, NULL, &p_count);
  platforms = (cl_platform_id *) malloc(sizeof(cl_platform_id) * p_count);
  err = clGetPlatformIDs(p_count, platforms, NULL);
  clErrorString(err);
  err = clGetDeviceIDs(platforms[1], CL_DEVICE_TYPE_GPU, 1, &device, NULL);
  clErrorString(err);
  context = clCreateContext(0, 1, &device, NULL, NULL, &err);
  clErrorString(err);
  queue = clCreateCommandQueue(context, device, 0, &err);
  clErrorString(err);

  stat(fname, &buf);
  k_source = (char *) malloc(buf.st_size + 1);

  f = fopen(fname, "r");
  fread(k_source, buf.st_size, 1, f);
  k_source[buf.st_size] = '\0';

  program = clCreateProgramWithSource(context, 1, (const char **) &k_source, NULL, &err);
  clErrorString(err);

  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  clErrorString(err);

  /* Check the build log. */
  {
    char *build_log;
    size_t log_size;

    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

    if (log_size > 1) {
      printf("Build log:\n*******\n");

      build_log = (char *) malloc(sizeof(char) * (log_size + 1));

      clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size, build_log, NULL);
      build_log[log_size] = '\0';

      printf("%s", build_log);
      printf("*******\n");

      free(build_log);
    }
    if (err != CL_SUCCESS) {
      exit(1);
    }
  }

  pscan_up = clCreateKernel(program, "pscan_up", &err);
  pscan_down = clCreateKernel(program, "pscan_down", &err);
  interleave = clCreateKernel(program, "interleave", &err);
  uninterleave = clCreateKernel(program, "uninterleave", &err);
  clErrorString(err);

  srand48(0);

  z = 0;

  /* Benchmark parallel scan for powers
  of two up to 28. */

  for (l = 2; l < 29; l++) {
    n = (unsigned long) pow(2, l);

    seq = (int *) malloc(sizeof(int) * n);
    x = (int *) malloc(sizeof(int) * n);
    rv = (int *) malloc(sizeof(int) * 2 * n);

    /* Randomly generate vector. */

    for (i = 0; i < n; i++) {
      x[i] = lrand48() % 100 - 50;
    }

    /* Compute scan sequentially (for checking). */

    seq[0] = x[0];
    for (i = 1; i < n; i++) {
      seq[i] = seq[i - 1] + x[i];
    }

    min = -1;

    /* Benchmark at valid chunk sizes from 2 to n,
    printing timing at optimum block size. */
    for (k = 2; k < n; k++) {
      /* At valid chunk sizes, perform parallel scan. */

      if (log(n) / log(k) == (int) (log(n) / log(k))) {
        s = (stack *) malloc(sizeof(stack));
        stack_init(s, n, k);

        d_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, s->size, NULL, NULL);
        d_x = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(int) * n, NULL, NULL);

        if (d_mem == NULL) {
          fprintf(stderr, "WHOOPS! Ran out of memory with request for %d bytes.\n", s->size);
          exit(0);
        }

        clEnqueueWriteBuffer(queue, d_x, CL_TRUE, 0, sizeof(int) * n, x, 0, NULL, NULL);

        clSetKernelArg(pscan_up, 0, sizeof(cl_mem), &d_mem);
        clSetKernelArg(pscan_up, 2, sizeof(unsigned long), &n);
        clSetKernelArg(pscan_up, 3, sizeof(int), &z);
        clSetKernelArg(pscan_up, 4, sizeof(int), &k);
        clSetKernelArg(pscan_up, 5, sizeof(int), &(s->size));

        clSetKernelArg(pscan_down, 0, sizeof(cl_mem), &d_mem);
        clSetKernelArg(pscan_down, 2, sizeof(unsigned long), &n);
        clSetKernelArg(pscan_down, 3, sizeof(int), &z);
        clSetKernelArg(pscan_down, 4, sizeof(int), &k);
        clSetKernelArg(pscan_down, 5, sizeof(int), &(s->size));

        clSetKernelArg(interleave, 0, sizeof(cl_mem), &d_x);
        clSetKernelArg(interleave, 1, sizeof(cl_mem), &d_mem);
        clSetKernelArg(interleave, 2, sizeof(unsigned long), &n);
        clSetKernelArg(interleave, 3, sizeof(int), &k);

        clSetKernelArg(uninterleave, 0, sizeof(cl_mem), &d_mem);
        clSetKernelArg(uninterleave, 1, sizeof(cl_mem), &d_x);
        clSetKernelArg(uninterleave, 2, sizeof(unsigned long), &n);
        clSetKernelArg(uninterleave, 3, sizeof(int), &k);

        unsigned long N;
        unsigned int sp;

        auto begin = std::chrono::high_resolution_clock::now();
        global_size = n;
        clEnqueueNDRangeKernel(queue, interleave, 1, NULL, &global_size, NULL, 0, NULL, NULL);
        clFinish(queue);
        auto time1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - begin).count();
        auto b_kern1 = std::chrono::high_resolution_clock::now();

        sp = 0;
        for (N = n; N > k; N /= k) {
          clSetKernelArg(pscan_up, 1, sizeof(unsigned int), &sp);
          clSetKernelArg(pscan_up, 2, sizeof(unsigned long), &N);
          global_size = N / k;
          clEnqueueNDRangeKernel(queue, pscan_up, 1, NULL, &global_size, NULL, 0, NULL, NULL);
          sp += N;
        }

        clFinish(queue);

        auto kern1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - b_kern1).count();
        auto b_kern2 = std::chrono::high_resolution_clock::now();

        for (N = k; N <= n; N *= k) {
          clSetKernelArg(pscan_down, 1, sizeof(unsigned int), &sp);
          clSetKernelArg(pscan_down, 2, sizeof(unsigned long), &N);
          global_size = N / k;
          clEnqueueNDRangeKernel(queue, pscan_down, 1, NULL, &global_size, NULL, 0, NULL, NULL);
          sp -= N * k;
        }

        clFinish(queue);
        auto kern2 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - b_kern2).count();
        auto begin2 = std::chrono::high_resolution_clock::now();
        global_size = n;
        clEnqueueNDRangeKernel(queue, uninterleave, 1, NULL, &global_size, NULL, 0, NULL, NULL);

        clFinish(queue);
        auto time2 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - begin2).count();
        auto time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - begin).count();

        // printf("[%d %lf (%d)]\n", n, (double) time / 1E6, k);

        /* Save best chunk size. */
        if (min == -1 || ((double) time / 1E6) < min) {
          min = (double) time / 1E6;
          min_int = (double) time1 / 1E6;
          min_deint = (double) time2 / 1E6;
          min_k1 = (double) kern1 / 1E6;
          min_k2 = (double) kern2 / 1E6;
          best_chunk = k;
        }

        clEnqueueReadBuffer(queue, d_x, CL_TRUE, 0, sizeof(int) * n, rv, 0, NULL, NULL);

        /* Check results against sequentially computed
           scan. */

        for (i = 0; i < n; i++) {
          if (rv[i] != seq[i]) {
            printf("Error.  Found with vector: \n");
            if (n < 128) {
              for (j = 0; j < n; j++) {
                printf("%d ", x[j]);
              }
              printf("\n");

              printf("Result:\n");

              for (j = 0; j < n; j++) {
                printf("%d ", rv[j]);
              }
              printf("\n");
            }

            exit(1);
          }
        }

        clReleaseMemObject(d_x);
        clReleaseMemObject(d_mem);
        free(s);
      }
    }

    printf("n: %lu s: %lf (cs: %d) int: %lf, deint: %lf, compute: %lf, k1: %lf, k2: %lf\n", n, min, best_chunk, min_int, min_deint, min - (min_int + min_deint), min_k1, min_k2);

    fflush(stdout);
    fflush(stderr);
    free(x);
    free(rv);
    free(seq);
  }

  free(platforms);
  clReleaseProgram(program);
  clReleaseKernel(pscan_up);
  clReleaseKernel(pscan_down);
  clReleaseKernel(interleave);
  clReleaseKernel(uninterleave);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);

  return 0;
}
