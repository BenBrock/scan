
/*

   iter_mic.c

   Benjamin Brock
   May, 2014

   Iterative implementation of a patterns-based
   parallel scan, stack implementation optimized
   for accelerators.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <cilk/cilk.h>
#include <mkl.h>

#define ilog(n) ((int) (log(n) + 0.5))
#define ipow(b, n) ((int) (pow(b, n) + 0.5))

#define pad_bytes 128

typedef struct stack {
  void *mem;
  void *sp;
  int size;
  int chunk_size;
} stack;

int *pscan(stack *s, int n, int z, int chunk_size);
void seq_scan(int *x, int n, int z);
int reduce(int *x, int n);

/* Initialize stack. */
void stack_init(stack *s, int n, int cs)
{
  int i;

  s->size = 0;

  for (i = n; i >= cs; i /= cs) {
    s->size += i + i % (pad_bytes / sizeof(int));
  }

  s->chunk_size = cs;

  s->mem = malloc(sizeof(int) * s->size);
  s->sp = s->mem;
}

void stack_destroy(stack *s)
{
  free(s->mem);
}

int *alloc(stack *s, int n)
{
  int *v;

  v = (int *) s->sp;
  s->sp += sizeof(int) * n + (sizeof(int) * n) % pad_bytes;

  return v;
}

int *pop(stack *s, int n)
{
  int *v;

  s->sp -= (n * sizeof(int) + (n * sizeof(int)) % pad_bytes);
  v = (int *) s->sp;

  return v;
}

int *grab(stack *s, int n)
{
  return (int *) (s->sp - (n * sizeof(int)) - (n * sizeof(int) % pad_bytes));
}

void print_stack(stack *s, int n)
{
  int i;

  printf("Stack.\n");
  for (i = 0; i < n; i++) {
    printf("%d ", ((int *) s->mem)[i]);
  }
  printf("\n");
}

int main(int argc, char **argv)
{
  int n;
  int i, j, k, l;
  int *x, *seq, *r;
  stack *s;
  double begin, end;

  double min;
  int best_chunk;

  srand48(time(0));

  /* Benchmark parallel scan for powers
     of two up to 28. */

  for (l = 2; l < 29; l++) {
    n = (int) pow(2, l);

    seq = (int *) malloc(sizeof(int) * n);
    x = (int *) malloc(sizeof(int) * n);

    /* Randomly generate vector. */

    for (i = 0; i < n; i++) {
      x[i] = lrand48() % 100 - 50;
      seq[i] = x[i];
    }

    /* Compute scan sequentially (for checking). */

    for (i = 1; i < n; i++) {
      seq[i] = seq[i - 1] + seq[i];
    }

    min = -1;

    /* Benchmark at valid chunk sizes from 2 to n,
       printing timing at optimum block size. */
    for (k = 2; k < n; k++) {

      /* At valid chunk sizes, perform parallel scan. */

      if (log(n) / log(k) == (int) (log(n) / log(k))) {
        s = (stack *) malloc(sizeof(stack));
        stack_init(s, n, k);

        ((int *) alloc(s, n))[0:n] = x[0:n];

        begin = dsecnd();

        r = pscan(s, n, 0, k);

        end = dsecnd();

        /* Save best chunk size. */
        if (min == -1 || end - begin < min) {
          min = end - begin;
          best_chunk = k;
        }

        /* Check results against sequentially computed
           scan. */

        for (i = 0; i < n; i++) {
          if (r[i] != seq[i]) {
            printf("n: %d, cs: %d, failed on %d\n", n, k, i);
            printf("AGGGHHH!!! ERROR.  Found with vector: \n");
            for (j = 0; j < n; j++) {
              printf("%d ", x[j]);
            }
            printf("\n");

            printf("Result:\n");
            for (j = 0; j < n; j++) {
              printf("%d ", x[j]);
            }
            printf("\n");
            exit(1);
          }
        }

        stack_destroy(s);
        free(s);
      }
    }

    printf("%d %lf (%d)\n", n, min, best_chunk);
    fflush(stdout);
    fflush(stderr);
    free(x);
    free(seq);
  }

  return 0;
}

/* Perform parallel scan. */
int *pscan(stack *s, int n, int z, int chunk_size)
{
  unsigned long long i, j;
  int *X, *sums, *sumscan;

  /* Build the stack, computing the sum of each layer. */

  X = grab(s, n);

  for (i = n; i > chunk_size; i /= chunk_size) {
    sums = alloc(s, i / chunk_size);

    cilk_for (j = 0; j < i / chunk_size; j++) {
      sums[j] = reduce(&X[j * chunk_size], chunk_size);
    }

    X = sums;
  }

  for (i = chunk_size; i <= n; i *= chunk_size) {
    sumscan = X;
    X = pop(s, i);

    cilk_for (j = 0; j < i / chunk_size; j++) {
      if (j > 0) {
        seq_scan(&X[j * chunk_size], chunk_size, sumscan[j - 1]);
      } else {
        seq_scan(&X[j * chunk_size], chunk_size, 0);
      }
    }
  }

  return X;
}

/* Sequential scan. */
void seq_scan(int *x, int n, int z)
{
  int i;

  x[0] += z;
  for (i = 1; i < n; i++) {
    x[i] += x[i - 1];
  }
}

/* Serial reduction. */
int reduce(int *x, int n)
{
  int i;
  int sum;

  sum = 0;

  for (i = 0; i < n; i++) {
    sum += x[i];
  }

  return sum;
}
