
/*

   iter.c

   Benjamin Brock
   May, 2014

   Iterative implementation of a patterns-based
   parallel scan.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <cilk/cilk.h>
#include <mkl.h>

int *pscan(int *x, int n, int z, int chunk_size);
int *seq_scan(int *x, int n, int z, int *rv);
int reduce(int *x, int n);

typedef struct stack {
  struct pancake *top;
} stack;

typedef struct pancake {
  struct pancake *next;
  int *val, *sums;
  int n, z, loc;
} pancake;

void stack_init(stack *s)
{
  s->top = NULL;
}

void push(stack *s, int *v, int size, int z, int loc, int *sums)
{
  pancake *p;

  p = (pancake *) malloc(sizeof(pancake));
  p->next = NULL;
  p->val = v;
  p->n = size;
  p->z = z;
  p->loc = loc;
  p->sums = sums;

  if (s->top == NULL) {
    s->top = p;
  } else {
    p->next = s->top;
    s->top = p;
  }
}

void print_stack(stack *s)
{
  pancake *itr;
  int i;

  itr = s->top;

  i = 0;

  if (itr == NULL) {
    printf("Stack is empty.\n");
  } else {
    while (itr != NULL) {
      printf("%d: %d\n", i, itr->n);
      itr = itr->next;
    }
  }
}

int *pop(stack *s, int *n, int *z, int *loc, int **sums)
{
  int *v;
  pancake *p;

  p = s->top;

  v = p->val;
  *z = p->z;
  *n = p->n;
  *loc = p->loc;
  *sums = p->sums;

  s->top = p->next;
  free(p);

  return v;
}

int *grab(stack *s, int *n, int *z, int *loc, int **sums)
{
  pancake *p;

  p = s->top;
  *z = p->z;
  *n = p->n;
  *loc = p->loc;
  *sums = p->sums;

  return p->val;
}

int main(int argc, char **argv)
{
  int n;
  int i, j, k, l;
  int *x, *seq, *r;
  double begin, end;

  double min;
  int best_chunk;

  srand48(time(0));

  /* Benchmark parallel scan for powers
     of two up to 28. */

  for (l = 2; l < 29; l++) {
    n = (int) pow(2, l);

    seq = (int *) malloc(sizeof(int) * n);
    x = (int *) malloc(sizeof(int) * n);

    /* Randomly generate vector. */

    for (i = 0; i < n; i++) {
      x[i] = lrand48() % 100 - 50;
      seq[i] = x[i];
    }

    /* Compute scan sequentially (for checking). */

    for (i = 1; i < n; i++) {
      seq[i] = seq[i - 1] + seq[i];
    }

    min = -1;

    /* Benchmark at valid chunk sizes from 2 to n,
       printing timing at optimum block size. */
    for (k = 2; k < n; k++) {

      /* At valid chunk sizes, perform parallel scan. */

      if (n % k == 0) {

        begin = dsecnd();

        r = pscan(x, n, 0, k);

        end = dsecnd();

        /* Save best chunk size. */
        if (min == -1 || end - begin < min) {
          min = end - begin;
          best_chunk = k;
        }

        /* Check results against sequentially computed
           scan. */

        for (i = 0; i < n; i++) {
          if (r[i] != seq[i]) {
            fprintf(stderr, "AGGGHHH!!! ERROR.  Found with vector: \n");
            for (j = 0; j < n; j++) {
              printf("%d ", x[j]);
            }
            printf("\n");
            exit(1);
          }
        }

        free(r);
      }
    }

    printf("%d %lf (%d)\n", n, min, best_chunk);
    fflush(stdout);
    fflush(stderr);
    free(x);
    free(seq);
  }

  return 0;
}

/* Perform parallel scan. */
int *pscan(int *x, int n, int z, int chunk_size)
{
  int i, j;
  int N, Z, loc;
  int *sums, *sumscan, *scan, **fsum, *rv;
  stack *s;

  s = (stack *) malloc(sizeof(stack));
  stack_init(s);

  rv = NULL;

  push(s, x, n, z, 0, NULL);

  x = grab(s, &N, &Z, &loc, &sums);

  /* Build the stack, computing the sum of each layer. */

  do {
    sums = (int *) malloc(sizeof(int) * (N / chunk_size));

    cilk_for (i = 0; i < N / chunk_size; i++) {
      sums[i] = reduce(&x[i * chunk_size], chunk_size);
    }

    s->top->sums = sums;

    /* Perform a recursive scan on the sums. */
    push(s, sums, N / chunk_size, 0, 0, sums);

    x = grab(s, &N, &Z, &loc, &sums);
  } while (N > chunk_size);

  /* Decompose the stack, computing the parallel scan
     of each layer. */
  while (s->top != NULL) {
    x = pop(s, &N, &Z, &loc, &sums);

    /* Base case, serially scan a chunk. */
    if (N <= chunk_size) {
      rv = (int *) malloc(sizeof(int) * N);
      rv = seq_scan(x, N, Z, rv);
      continue;
    }

    sumscan = rv;

    free(sums);

    /* Perform a sequentially scan on each chunk,
       using the appropriate offset from the sums
       scan. */
    rv = (int *) malloc(sizeof(int) * N);

    cilk_for (i = 0; i < N / chunk_size; i++) {
      if (i > 0) {
        seq_scan(&x[i * chunk_size], chunk_size, sumscan[i - 1], &rv[i * chunk_size]);
      } else {
        seq_scan(&x[i * chunk_size], chunk_size, 0, &rv[i * chunk_size]);
      }
    }

    free(sumscan);
  }

  return rv;
}

/* Sequential scan. */
int *seq_scan(int *x, int n, int z, int *rv)
{
  int i;

  rv[0] = x[0] + z;
  for (i = 1; i < n; i++) {
    rv[i] = rv[i - 1] + x[i];
  }

  return rv;
}

/* Serial reduction. */
int reduce(int *x, int n)
{
  int i;
  int sum;

  sum = 0;

  for (i = 0; i < n; i++) {
    sum += x[i];
  }

  return sum;
}
