
/*
  pscan.c

  Benjamin Brock
  May, 2014

  Cilk implementation of a general patterns-based
  parallel scan algorithm.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <cilk/cilk.h>
#include <mkl.h>

int *pscan(int *x, int n, int z, int chunk_size);
int reduce(int *x, int n);

int main(int argc, char **argv)
{
  int n;
  int i, j, k, l;
  int *x, *seq, *r;
  double begin, end;

  double min;
  int best_chunk;

  srand48(time(0));

  /* Benchmark parallel scan for powers
     of two up to 28. */

  for (l = 2; l < 29; l++) {
    n = (int) pow(2, l);

    x = (int *) malloc(sizeof(int) * n);
    seq = (int *) malloc(sizeof(int) * n);

    /* Randomly generate vector. */
    for (i = 0; i < n; i++) {
      x[i] = lrand48() % 100 - 50;
      seq[i] = x[i];
    }

    /* Compute scan sequentially (for checking). */

    for (i = 1; i < n; i++) {
      seq[i] = seq[i - 1] + seq[i];
    }

    min = -1;

    /* Benchmark at valid chunk sizes from 2 to n,
       printing timing at optimum block size. */
    for (k = 2; k < n; k++) {

      /* At valid chunk sizes, perform parallel scan. */

      if (n % k == 0) {
	begin = dsecnd();

	r = pscan(x, n, 0, k);

	end = dsecnd();

        /* Save best chunk size. */
	if (min == -1 || end - begin < min) {
	  min = end - begin;
	  best_chunk = k;
	}

	/* Check result against sequentially computed
           scan. */

	for (i = 0; i < n; i++) {
	  if (r[i] != seq[i]) {
	    fprintf(stderr, "Error. Found with vector: \n");
	    for (j = 0; j < n; j++) {
	      printf("%d ", x[j]);
	    }
	    printf("\n");
	    exit(1);
	  }
	}

	free(r);
      }
    }

    free(x);
    free(seq);

    printf("%d %lf\n", n, min);
    fflush(stdout);
    fflush(stderr);
  }

  return 0;
}

/* Perform parallel scan. */
int *pscan(int *x, int n, int z, int chunk_size)
{
  int i, j;
  int *sums, *sumscan, *scan, **fsum, *rv;

  /* Base case, serially scan a chunk. */
  if (n <= chunk_size) {
    scan = (int *) malloc(sizeof(int) * n);

    scan[0] = x[0] + z;
    for (i = 1; i < n; i++) {
      scan[i] = x[i] + scan[i - 1];
    }

    return scan;
  }

  sums = (int *) malloc(sizeof(int) * (n / chunk_size));

  /* Reduce each chunk of the array. */
  cilk_for (i = 0; i < n / chunk_size; i++) {
    sums[i] = reduce(&x[i * chunk_size], chunk_size);
  }

  /* Perform a scan on the sums. */
  sumscan = pscan(sums, n / chunk_size, 0, chunk_size);

  free(sums);

  fsum = (int **) malloc(sizeof(int *) * (n / chunk_size));

  /* Perform a recursive scan on each chunk, using
     the appropriate offset from the sums scan. */
  cilk_for (i = 0; i < n / chunk_size; i++) {
    if (i > 0) {
      fsum[i] = pscan(&x[i * chunk_size], chunk_size, sumscan[i - 1], chunk_size);
    } else {
      fsum[i] = pscan(&x[i * chunk_size], chunk_size, 0, chunk_size);
    }
  }

  free(sumscan);

  rv = (int *) malloc(sizeof(int) * n);

  /* Join the arrays. */

  for (i = 0; i < n / chunk_size; i++) {
    cilk_for (j = 0; j < chunk_size; j++) {
      rv[i * chunk_size + j] = fsum[i][j];
    }
  }

  cilk_for (i = 0; i < n / chunk_size; i++) {
    free(fsum[i]);
  }

  free(fsum);

  return rv;
}

/* Serial reduction. */
int reduce(int *x, int n)
{
  int i;
  int sum;

  sum = 0;

  for (i = 0; i < n; i++) {
    sum += x[i];
  }

  return sum;
}
