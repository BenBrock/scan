
#define reorder(i, cs, n) ((i) / (cs)) + ((i) % (cs)) * ((n) / (cs))

__kernel void pscan_up(__global int *mem, const unsigned int sp, const unsigned long n, const int z, const int chunk_size, const int size)
{
  int i, sum, stride;
  __global int *X, *sums;

  X = mem + sp;
  sums = X + n;

  stride = n / chunk_size;

  sum = 0;

  for (i = get_global_id(0); i < n; i += stride) {
    sum += X[i];
  }

  sums[reorder(get_global_id(0), chunk_size, stride)] = sum;
}

__kernel void pscan_down(__global int *mem, const unsigned int sp, const unsigned long n, const int z, const int chunk_size, const int size)
{
  int i, stride;
  __global int *X, *sumscan;

  X = mem + sp;
  sumscan = X + n;

  stride = n / chunk_size;

  if (get_global_id(0) > 0) {
    X[get_global_id(0)] += sumscan[reorder(get_global_id(0) - 1, chunk_size, stride)];
  }

  for (i = get_global_id(0) + stride; i < n; i += stride) {
    X[i] += X[i - stride];
  }
}

__kernel void interleave(__global int *src, __global int *dest, const unsigned long n, const int chunk_size)
{
  dest[reorder(get_global_id(0), chunk_size, n)] = src[get_global_id(0)];
}

__kernel void uninterleave(__global int *src, __global int *dest, const unsigned long n, const int chunk_size)
{
  dest[get_global_id(0)] = src[reorder(get_global_id(0), chunk_size, n)];
}


#define BLOCK_DIM 16
// interleave: global = bs x n / bs;  local = BLOCK_DIM x BLOCK_DIM
// uninterleave: global = n / bs x bs; local = BLOCK_DIM x BLOCK_DIM
kernel void transpose_local(const global DataType * restrict src,
                                  global DataType * restrict dest)
{
  local DataType cache[(BLOCK_DIM+1)*BLOCK_DIM];

  unsigned int tidx = get_global_id(0);
  unsigned int tidy = get_global_id(1);

  // fetch 
  // this will crash if the global, local and block sizes are not compatible
  unsigned int index_in = tidy * get_global_size(0) + tidx;
  cache[get_local_id(1)*(BLOCK_DIM+1)+get_local_id(0)] = src[index_in];

  barrier(CLK_LOCAL_MEM_FENCE);

  // rotate and commit
  tidx = get_group_id(1) * BLOCK_DIM + get_local_id(0);
  tidy = get_group_id(0) * BLOCK_DIM + get_local_id(1);
  unsigned int index_out = tidy * get_global_size(1) + tidx;
  dest[index_out] = cache[get_local_id(0)*(BLOCK_DIM+1)+get_local_id(1)];
}


