
/*

  scan.c

  Benjamin Brock
  May, 2014

  Cilk implementation of the parallel scan
  algorithm by Blelloch discussed here
  http://http.developer.nvidia.com/GPUGems3/gpugems3_ch39.html.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <cilk/cilk.h>
#include <mkl.h>

#define pow2(n) (1 << (n))

unsigned int loge2(int n)
{
  unsigned int v = n;
  unsigned int r = 0;

  while (v >>= 1) {
    r++;
  }

  return r;
}

int main(int argc, char **argv)
{
  int size;

  int i, j, k, l;
  int temp;
  int *d, *seq, *mem;
  double begin, end;

  srand48(time(0));

  /* Benchmark parallel scan for powers
     of two up to 28. */

  for (l = 2; l < 29; l++) {
    size = pow2(l);

    d = (int *) malloc(sizeof(int) * size);
    mem = (int *) malloc(sizeof(int) * size);
    seq = (int *) malloc(sizeof(int) * size);

    /* Randomly generate vector. Algorithm is
       destructive, so save copy for checking. */
    for (i = 0; i < size; i++) {
      d[i] = lrand48() % 100 - 50;
      mem[i] = d[i];
    }

    /* Compute scan sequentially (for checking). */

    seq[0] = 0;
    for (i = 1; i < size; i++) {
      seq[i] = seq[i - 1] + d[i - 1];
    }

    begin = dsecnd();

    /* Perform up-sweep. */

    for (i = 0; i < loge2(size); i++) {
      cilk_for (j = 0; j < size; j += pow2(i + 1)) {
        d[j + pow2(i + 1) - 1] = d[j + pow2(i) - 1] + d[j + pow2(i + 1) - 1];
      }
    }

    /* Perform down-sweep. */

    d[size - 1] = 0;

    for (i = loge2(size) - 1; i >= 0; i--) {
      cilk_for (j = 0; j < size; j += pow2(i + 1)) {
        temp = d[j + pow2(i) - 1];
        d[j + pow2(i) - 1] = d[j + pow2(i + 1) - 1];
        d[j + pow2(i + 1) - 1] = temp + d[j + pow2(i + 1) - 1];
      }
    }

    end = dsecnd();

    printf("%d %lf\n", size, end - begin);

    /* Check result against sequentially computed
       scan. */

    for (i = 0; i < size; i++) {
      if (d[i] != seq[i]) {
        fprintf(stderr, "Error. Found with vector:\n");
        for (k = 0; k < size; k++) {
          printf("%d ", mem[k]);
        }
        printf("\n");

        exit(1);
      }
    }
    free(d);
    free(seq);
    free(mem);
  }

  return 0;
}
