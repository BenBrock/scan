#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cilk/cilk.h>

int fact(int n);

typedef struct stack {
  void *data;
  int size;
  int msize;
} stack;
 
void stack_init(stack *s, int msize)
{
  s->data = (void *) malloc(msize);
  s->size = 0;
  s->msize = msize;
}
 
void push(stack *s, void *v, int size)
{
  if (s->size + size > s->msize) {
    fprintf(stderr, "Stack overflow (ours)\n");
    exit(1);
  } else {
    memcpy(&(s->data[size]), v, size);
    s->size += size;
  }
}
 
void *pop(stack *s, int size)
{
  void *v;

  if (s->size < size) {
    fprintf(stderr, "Stack over-reach (ours, pop)\n");
    exit(1);
  } else {
    v = &(s->data[s->size - size]);
    s->size -= size;
    return v;
  }
}

void *grab(stack *s, int size)
{
  if (s->size < size) {
    fprintf(stderr, "Stack over-reach (ours, grab)\n");
    exit(1);
  } else {
    return &(s->data[s->size - size]);
  }
}

void kill_stack(stack *s)
{
  free(s->data);
}

int main(int argc, char **argv)
{
  int i;

  for (i = 0; i < 12; i++) {
    printf("%d\n", fact(i));
  }

  stack *s;

  s = (stack *) malloc(sizeof(stack));

  int size;

  /* 8 MB stack */
  size = (int) pow(2, 23);

  stack_init(s, size);

  printf("%d\n", factorial(s, 10));

  kill_stack(s);

  return 0;
}


int factorial(stack *s, int n)
{
  int rv = 0; // the value we will ultimately return from this function
 
    // the various valid labels we can have in our "program"
  typedef enum {
    ENTRY_POINT,
    PRE_FUNCTION,
    POST_FUNCTION,
    EXIT_POINT,
    FUNCTION,
    PRE_RECURSIVE_FUNCTION,
    POST_RECURSIVE_FUNCTION
  } labels;
 
  labels pc = ENTRY_POINT; // the program counter
  int done = 0;             // has "program" execution completed
 
  int rv_reg;        // a return value "register"


  int tmp;
 
  // the offset on the pStack frame of the passed arguments
  const int return_addrOffset = 0;
  const int numberArgOffset = 1;
 
  while (!done) {
    switch (pc) {
    // Main part of "program"
 
      case ENTRY_POINT:
        // do some stuff
 
        case PRE_FUNCTION:
          // get ready for function "call"
 
          // push variable(s) onto pStack
          push(s, &n, sizeof(int));
 
          // push "program counter" return label
          tmp = (int) POST_FUNCTION;
          push(s, &tmp, sizeof(int));
 
          // "jump" to function
          pc = FUNCTION;
          break;
 
        case POST_FUNCTION:
          // clean up pStack
          pop(s, 2 * sizeof(int));
 
          // use the returned value
          rv = rv_reg;
 
          case EXIT_POINT:
            // mark that we've done
            done = 1;
            break;
 
          // Function part of "program"
 
          case FUNCTION:
          {
            // where to return to
            labels return_addr = (labels) *((int *) grab(s, return_addrOffset * sizeof(int)));
 
            // get variable(s) from pStack
            int local_num = *((int *) grab(s, numberArgOffset * sizeof(int)));
 
            // exit condition
            if (local_num <= 1) {
              // "return" a value
              rv_reg = 1;
 
              // "jump" to return label
              pc = return_addr;
              break;
            }
          }
 
          case PRE_RECURSIVE_FUNCTION:
            // get ready for function "call"
 
            // push variable(s) onto pStack
            push(s, &(*((int *) grab(s, numberArgOffset * sizeof(int)))) - 1, sizeof(int));
 
            // push "program counter" return label
            tmp = POST_RECURSIVE_FUNCTION;
            push(s, &tmp, sizeof(int));
 
            // "jump" to function
            pc = FUNCTION;
            break;
 
          case POST_RECURSIVE_FUNCTION:
          {
            // clean up pStack
            pop(s, 2 * sizeof(int));
 
            // do some work in the "function"
            //
            int local_rv = *((int *) grab(s, numberArgOffset * sizeof(int))) * rv_reg;
 
            // "return" a value
            rv_reg = local_rv;
 
            // "jump" to return label
            pc = *((labels *) grab(s, return_addrOffset));
            break;
          }
 
          default:
            /* This shouldn't occur. */
            fprintf(stderr, "ERROR, reached default.\n");
            exit(1);
        }
    }
 
    // return the final calculated value
    return rv;
}
 
int fact(int n)
{
  if (n == 0 || n == 1) {
    return 1;
  } else {
    return n * fact(n - 1);
  }
}
