
/*

  scan.c

  Benjamin Brock
  May, 2014

  Cilk implementation of the parallel scan
  algorithm by Blelloch discussed here
  http://http.developer.nvidia.com/GPUGems3/gpugems3_ch39.html.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

int main(int argc, char **argv)
{
  int size;

  int i, j, k, l;
  int cs;
  int temp;
  int *d, *seq, *mem;
  double begin, end;

  srand48(time(0));

  /* Benchmark parallel scan for powers
     of two up to 28. */

  cs = 2;

  end = 2.0;
  begin = 1.0;

  for (l = 2; l < 29; l++) {
    size = (int) (pow(cs, l) + 0.5);

    d = (int *) malloc(sizeof(int) * size);
    mem = (int *) malloc(sizeof(int) * size);
    seq = (int *) malloc(sizeof(int) * size);

    /* Randomly generate vector. Algorithm is
       destructive, so save copy for checking. */
    for (i = 0; i < size; i++) {
      d[i] = lrand48() % 100 - 50;
      mem[i] = d[i];
    }

    /* Compute scan sequentially (for checking). */

    seq[0] = 0;
    for (i = 1; i < size; i++) {
      seq[i] = seq[i - 1] + d[i - 1];
    }

    /* Perform up-sweep. */

    for (i = 0; i < (int) (log(size) / log(cs)); i++) {
      for (j = 0; j < size; j += (int) pow(cs, i + 1)) {
        d[j + (int) pow(cs, i + 1) - 1] = d[j + (int) pow(cs, i) - 1] + d[j + (int) pow(cs, i + 1) - 1];
      }
    }

    /* Perform down-sweep. */

    d[size - 1] = 0;

    for (i = (int) (log(size) / log(cs)) - 1; i >= 0; i--) {
      for (j = 0; j < size; j += (int) pow(cs, i + 1)) {
        temp = d[j + (int) pow(cs, i) - 1];
        d[j + (int) pow(cs, i) - 1] = d[j + (int) pow(cs, i + 1) - 1];
        d[j + (int) pow(cs, i + 1) - 1] = temp + d[j + (int) pow(cs, i + 1) - 1];
      }
    }

    printf("%d %lf\n", size, end - begin);

    /* Check result against sequentially computed
       scan. */

    for (i = 0; i < size; i++) {
      if (d[i] != seq[i]) {
        fprintf(stderr, "Error. Found with vector:\n");
        for (k = 0; k < size; k++) {
          printf("%d ", mem[k]);
        }
        printf("\n");

        printf("Result:\n");

        for (k = 0; k < size; k++) {
          printf("%d ", d[k]);
        }
        printf("\n");

        exit(1);
      }
    }
    free(d);
    free(seq);
    free(mem);
  }

  return 0;
}
