
/*
   tbb_scan.cpp

   Benjamin Brock
   May, 2014

   Harness for testing TBB's implementation of
   parallel scan.

 */

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <cmath>
#include <mkl.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/blocked_range.h>
#include <tbb/parallel_scan.h>
#include <tbb/tick_count.h>

using namespace std;
using namespace tbb;

template <class T>
class Body 
{
  T reduced_result;
  T *const y;
  const T *const x;

  public:

  Body(T y_[], const T x_[]) : reduced_result(0), x(x_), y(y_) {}

  T get_reduced_result() const { return reduced_result; }

  template <typename Tag>
    void operator()(const blocked_range <int> &r, Tag) 
    {
      T temp = reduced_result;

      int rend = r.end();
      for (int i = r.begin(); i < rend; ++i) {
        temp = temp + x[i];
        if (Tag::is_final_scan())
          y[i] = temp;
      }

      reduced_result = temp;
    }

  Body(Body &b, split) : x(b.x), y(b.y), reduced_result(0) {}

  void reverse_join(Body &a) 
  {
    reduced_result = a.reduced_result + reduced_result;
  }

  void assign(Body &b) 
  {   
    reduced_result = b.reduced_result;
  }
};

  template <class T>
float pscan(const T *x, int n, T *y) 
{
  Body <int> body(y,x);
  parallel_scan(blocked_range <int> (0, n), body, auto_partitioner());
  return body.get_reduced_result();
}

int main(int argc, char **argv)
{
  int n;
  int i, j, k, l;
  int *x, *seq, *r;
  double begin, end;

  double min;
  int best_chunk;

  srand48(time(0));

  for (l = 2; l < 29; l++) {
    n = (int) pow(2, l);

    x = (int *) malloc(sizeof(int) * n);
    r = (int *) malloc(sizeof(int) * n);
    seq = (int *) malloc(sizeof(int) * n);

    for (i = 0; i < n; i++) {
      x[i] = lrand48() % 100 - 50;
      seq[i] = x[i];
    }

    for (i = 1; i < n; i++) {
      seq[i] = seq[i - 1] + seq[i];
    }

    begin = dsecnd();

    pscan(x, n, r);

    end = dsecnd();

    for (i = 0; i < n; i++) {
      if (r[i] != seq[i]) {
        fprintf(stderr, "Error. Found with vector: \n");
        for (j = 0; j < n; j++) {
          printf("%d ", x[j]);
        }
        printf("\n");
        exit(1);
      }
    }

    free(r);
    free(x);
    free(seq);

    printf("%d %lf\n", n, end - begin);
    fflush(stdout);
    fflush(stderr);
  }

  return 0;
}
